package com.ashish.testapplicatiom;

public class Proucts {
    int id;
    String name, desc;
    byte[] productphoto;
    double regularprice, saleprice;

    public Proucts(int id, String name, String desc, byte[] productphoto, double regularprice, double saleprice) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.productphoto = productphoto;
        this.regularprice = regularprice;
        this.saleprice = saleprice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public byte[] getProductphoto() {
        return productphoto;
    }

    public void setProductphoto(byte[] productphoto) {
        this.productphoto = productphoto;
    }

    public double getRegularprice() {
        return regularprice;
    }

    public void setRegularprice(double regularprice) {
        this.regularprice = regularprice;
    }

    public double getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(double saleprice) {
        this.saleprice = saleprice;
    }
}
