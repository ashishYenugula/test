package com.ashish.testapplicatiom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity   implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_products:

                startActivity(new Intent(this, AddProductsActivity.class));

                break;
            case R.id.view_products:

                startActivity(new Intent(this, ProductsActivity.class));

                break;
        }
    }


}
