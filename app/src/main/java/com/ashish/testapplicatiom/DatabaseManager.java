package com.ashish.testapplicatiom;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ProductsDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "products";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DESC = "description";
    private static final String COLUMN_REG_PRICE = "regularprice";
    private static final String COLUMN_SALE_PRICE = "saleprice";
    private static final String COLUMN_IMAGE = "productphoto";

    public DatabaseManager(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + " (\n" +
                "    " + COLUMN_ID + " INTEGER NOT NULL CONSTRAINT products_pk PRIMARY KEY AUTOINCREMENT,\n" +
                "    " + COLUMN_NAME + " varchar(200) NOT NULL,\n" +
                "    " + COLUMN_DESC + " varchar(200) NOT NULL,\n" +
                "    " + COLUMN_REG_PRICE + " double NOT NULL,\n" +
                "    " + COLUMN_SALE_PRICE + " double NOT NULL,\n" +
                "    " + COLUMN_IMAGE + " BLOB NOT NULL\n" +
                ");";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        db.execSQL(sql);
        onCreate(db);
    }

    //INSERT DATA
    boolean addProduct(String name, String desc, byte[] productphoto, String regularprice, String saleprice)  throws SQLiteException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_DESC, desc);
        contentValues.put(COLUMN_IMAGE, productphoto);
        contentValues.put(COLUMN_REG_PRICE, regularprice);
        contentValues.put(COLUMN_SALE_PRICE, saleprice);
        SQLiteDatabase db = getWritableDatabase();
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    Cursor getAllProducts() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    //UPDATE DATA
    boolean updateProduct(int id, String name, String desc, double regularprice) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_DESC, desc);
       // contentValues.put(COLUMN_IMAGE, productphoto);
        contentValues.put(COLUMN_REG_PRICE, regularprice);
       // contentValues.put(COLUMN_SALE_PRICE, saleprice);
        return db.update(TABLE_NAME, contentValues, COLUMN_ID + "=?", new String[]{String.valueOf(id)}) == 1;
    }

    //DELETE DATA
    boolean deleteProduct(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_NAME, COLUMN_ID + "=?", new String[]{String.valueOf(id)}) == 1;
    }

}
