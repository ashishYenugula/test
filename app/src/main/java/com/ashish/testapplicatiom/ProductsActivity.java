package com.ashish.testapplicatiom;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {
    List<Proucts> prouctsList;
    ListView listView;
    DatabaseManager mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        mDatabase = new DatabaseManager(this);

        prouctsList = new ArrayList<>();
        listView =  findViewById(R.id.list_view);

        loadProductsFromDatabase();
    }

    private void loadProductsFromDatabase() {
        Cursor cursor = mDatabase.getAllProducts();

        if (cursor.moveToFirst()) {
            do {
                prouctsList.add(new Proucts(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getDouble(4),
                        cursor.getDouble(5)
                ));
            } while (cursor.moveToNext());

            //passing the databasemanager instance this time to the adapter
            ProductAdapter adapter = new ProductAdapter(this, R.layout.list_layout_product, prouctsList, mDatabase);
            listView.setAdapter(adapter);
        }
    }
}
