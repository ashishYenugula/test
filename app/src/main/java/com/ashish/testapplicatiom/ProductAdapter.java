package com.ashish.testapplicatiom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ProductAdapter extends ArrayAdapter<Proucts> {
    Context context;
    int resource;
    List<Proucts> productsList;
    DatabaseManager mDatabase;

    public ProductAdapter(@NonNull Context context, int resource, List<Proucts> productsList, DatabaseManager mDatabase) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        this.productsList = productsList;
        this.mDatabase = mDatabase;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);

        final Proucts products = productsList.get(position);


        TextView textViewName = view.findViewById(R.id.textViewName);
        TextView textViewDesc = view.findViewById(R.id.textViewDescription);
        TextView textViewprice = view.findViewById(R.id.textViewprice);
        ImageView prodImage = view.findViewById(R.id.image_view);


        textViewName.setText(products.getName());
        textViewDesc.setText(products.getDesc());
        textViewprice.setText(String.valueOf(products.getRegularprice()));
        byte[] image = products.getProductphoto();
        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
        prodImage.setImageBitmap(bmp);


        Button buttonDelete = view.findViewById(R.id.buttonDeleteproduct);
        Button buttonEdit = view.findViewById(R.id.buttonEditproduct);

        view.findViewById(R.id.buttonDeleteproduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct(products);
            }
        });

        view.findViewById(R.id.buttonEditproduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct(products);
            }
        });


        return view;
    }

    private void updateProduct(final Proucts products) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_update_product, null);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        final EditText editTextName = view.findViewById(R.id.editTextName);
        final EditText editTextprice = view.findViewById(R.id.editTextprice);
        final EditText editTextdes = view.findViewById(R.id.editTextdes);


        editTextName.setText(products.getName());
        editTextprice.setText(String.valueOf(products.getRegularprice()));


        view.findViewById(R.id.buttonUpdateproduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = editTextName.getText().toString().trim();
                String price = editTextprice.getText().toString().trim();
                String desc = editTextdes.getText().toString().trim();

                if (name.isEmpty()) {
                    editTextName.setError("Name can't be empty");
                    editTextName.requestFocus();
                    return;
                }
                if (desc.isEmpty()) {
                    editTextdes.setError("Description can't be empty");
                    editTextdes.requestFocus();
                }
                if (price.isEmpty()) {
                    editTextprice.setError("price can't be empty");
                    editTextprice.requestFocus();
                    return;
                }


                //calling the update method from database manager instance
                if (mDatabase.updateProduct(products.getId(), name, desc, Double.valueOf(price))) {
                    Toast.makeText(context, "Product Updated", Toast.LENGTH_SHORT).show();
                    loadProductsFromDatabaseAgain();
                }
                alertDialog.dismiss();
            }
        });
    }

    private void deleteProduct(final Proucts products) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //calling the delete method from the database manager instance
                if (mDatabase.deleteProduct(products.getId()))
                    loadProductsFromDatabaseAgain();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void loadProductsFromDatabaseAgain() {
        //calling the read method from database instance
        Cursor cursor = mDatabase.getAllProducts();

        productsList.clear();
        if (cursor.moveToFirst()) {
            do {
                productsList.add(new Proucts(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getBlob(3),
                        cursor.getDouble(4),
                        cursor.getDouble(5)
                ));
            } while (cursor.moveToNext());
        }
        notifyDataSetChanged();
    }
}
