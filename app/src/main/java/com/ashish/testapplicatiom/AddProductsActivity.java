package com.ashish.testapplicatiom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class AddProductsActivity extends AppCompatActivity {
    EditText editTextName, editTextDesc, editTextprice, editTextSalePrice;
    TextInputLayout image;
    Button buttonAddProduct;
    DatabaseManager mDatabase;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    byte[] inputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_products);
        editTextName = findViewById(R.id.editTextName);
        editTextDesc = findViewById(R.id.editTextdesc);
        editTextprice = findViewById(R.id.editTextprice);
        editTextSalePrice = findViewById(R.id.editTextsaleprice);
        image = findViewById(R.id.image_layout);
        buttonAddProduct = findViewById(R.id.buttonAddProduct);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opengallary();
            }
        });
        buttonAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String price = editTextprice.getText().toString().trim();
                String desc = editTextDesc.getText().toString();
                String saleprice = editTextSalePrice.getText().toString();
                InputStream iStream = null;
                try {
                    iStream = getContentResolver().openInputStream(imageUri);
                    inputData = getBytes(iStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (validate(name, price, desc, inputData, saleprice)) {
                    mDatabase.addProduct(name, desc, inputData, price, saleprice);
                }

            }
        });

    }

    private boolean validate(String name, String price, String desc, byte[] imageUri, String saleprice) {
        if (name.isEmpty()) {
            editTextName.setError("Please enter a name");
            editTextName.requestFocus();
            return false;
        }
        if (desc.isEmpty()) {
            editTextDesc.setError("Please enter a description");
            editTextDesc.requestFocus();
            return false;
        }

        if (price.isEmpty() || Integer.parseInt(price) <= 0) {
            editTextprice.setError("Please enter price");
            editTextprice.requestFocus();
            return false;
        }
        if (saleprice.isEmpty() || Integer.parseInt(saleprice) <= 0) {
            editTextSalePrice.setError("Please enter price");
            editTextSalePrice.requestFocus();
            return false;
        }


        return true;
    }

    private void opengallary() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();

        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
}
